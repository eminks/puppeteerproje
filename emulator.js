const puppeteer = require('puppeteer');
const devices = require('puppeteer/DeviceDescriptors');

(async () => {
  const tarayici = await puppeteer.launch();
  const sekme = await tarayici.newPage();
  await sekme.emulate(devices['Nexus 6']);
  await sekme.goto("https://eminkose.com");
  await sekme.screenshot({path: 'demo.png', fullPage: true});
  const title = await sekme.title();
  const description = await sekme.evaluate(() =>
  document.querySelector("meta[name='description']").getAttribute("content"));
  var json = [ "demo.png", title, description];
  console.log(json);
  await tarayici.close();
})();
